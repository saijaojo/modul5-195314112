/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo_modul5_praktikum;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author asus
 */

public class Tugas1 extends JFrame implements ActionListener{

    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 210;
    private static final int LABEL_WIDTH = 80;
    private static final int LABEL_HEIGHT = 30;
    private static final int FIELD_WIDTH = 150;
    private static final int FIELD_HEIGHT = 30;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JLabel label_Panjang;
    private JLabel label_Lebar;
    private JLabel label_Luas;
    private JTextField text_Panjang;
    private JTextField text_Lebar;
    private JTextField text_Luas;
    private JButton button_Hitung;

    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
        
    }

    public Tugas1() {
        Container contenPane = getContentPane();
        contenPane.setLayout(null);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Luas Tanah");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        setResizable(false);

        label_Panjang = new JLabel("Panjang (m)");
        label_Panjang.setBounds(20, 15, LABEL_WIDTH, LABEL_HEIGHT);
        contenPane.add(label_Panjang);
        
        label_Lebar = new JLabel("Lebar (m)");
        label_Lebar.setBounds(20, 50, LABEL_WIDTH, LABEL_HEIGHT);
        contenPane.add(label_Lebar);
        
        label_Luas = new JLabel("Luas (m2)");
        label_Luas.setBounds(20, 85, LABEL_WIDTH, LABEL_HEIGHT);
        contenPane.add(label_Luas);
        
        text_Panjang = new JTextField();
        text_Panjang.setBounds(140, 15, FIELD_WIDTH, FIELD_HEIGHT);
        contenPane.add(text_Panjang);
        
        text_Lebar = new JTextField();
        text_Lebar.setBounds(140, 50, FIELD_WIDTH, FIELD_HEIGHT);
        contenPane.add(text_Lebar);
        
        text_Luas = new JTextField();
        text_Luas.setBounds(140, 85, FIELD_WIDTH, FIELD_HEIGHT);
        contenPane.add(text_Luas);
        
        button_Hitung = new JButton("Hitung");
        button_Hitung.setBounds(180, 120, BUTTON_WIDTH, BUTTON_HEIGHT);
        button_Hitung.addActionListener(this);
        contenPane.add(button_Hitung);
        
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        int bil1;
        int bil2;
        int luas;
        
        try {
        bil1 = Integer.parseInt(text_Panjang.getText());
        bil2 = Integer.parseInt(text_Lebar.getText());
        luas = bil1 * bil2;
        text_Luas.setText(Integer.toString(luas)); 
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,"Maaf, hanya integer yang di "
                    + "perbolehkan!","", JOptionPane.ERROR_MESSAGE);
            text_Panjang.setText("");
            text_Lebar.setText("");
        }
       setVisible(true);
       setLocation(150, 230);
       
    }
}
