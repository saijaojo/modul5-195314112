/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iiic;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author asus
 */
public class Nomor1 extends JFrame {

    private JPanel panel1;
    private JMenuBar bar;
    private JMenu file;
    private JMenu edit;
    private JMenuItem tampil1;
    private JMenuItem tampil2;

    public Nomor1() {
        initComponents();

    }

    public static void main(String[] args) {
        Nomor1 dialog = new Nomor1();
        dialog.setResizable(false);
        dialog.setVisible(true);
    }

    private void initComponents() {
        this.setLayout(null);
        this.setSize(600, 400);
        this.setLocation(100, 100);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("                                                                   "
                + "Frame Pertama");
        this.setVisible(true);

        bar = new JMenuBar();
        bar.setBounds(5, 0, 574, 20);
        file = new JMenu("File");
        tampil1 = new JMenuItem("Tampil1");
        tampil2 = new JMenuItem("Tampil2");
        file.setBounds(0, 0, 50, 20);
        tampil1.setBounds(0, 0, 50, 20);
        tampil2.setBounds(0, 0, 50, 20);
        file.add(tampil1);
        file.add(tampil2);
        bar.add(file);
        this.add(bar);

        edit = new JMenu("edit");
        edit.setBounds(51, 0, 50, 20);
        bar.add(edit);
        this.add(bar);

        panel1 = new JPanel();
        panel1.setBounds(5, 0, 574, 354);
        panel1.setBackground(Color.pink);
        this.add(panel1);
    }
}
