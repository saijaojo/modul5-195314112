/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iiic;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

/**
 *
 * @author asus
 */
public class Nomor5 extends JDialog {

    private static final int DIALOG_WIDTH = 500;
    private static final int DIALOG_HEIGHT = 140;
    private static final int DIALOG_X_WIDTH = 150;
    private static final int DIALOG_Y_HEIGHT = 250;

    public static void main(String[] args) {
        Nomor5 dialog = new Nomor5();
        dialog.setVisible(true);

    }

    public Nomor5() {
        Container contentPane;
        JPanel panell, panel2, panel3, panel4;
        JButton left, right;
        JCheckBox centered, bold, italic;
        JTextArea text;
        JRadioButton red, green, blue;

        setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
        setTitle("CheckBoxDemo");
        setLocation(DIALOG_X_WIDTH, DIALOG_Y_HEIGHT);

        contentPane = getContentPane();
        contentPane.setBackground(Color.white);
        contentPane.setLayout(new BorderLayout());

       LineBorder line = new LineBorder(Color.black);

        panell = new JPanel(new GridLayout(3, 3));
        centered = new JCheckBox("Centered");
        panell.add(centered);
        bold = new JCheckBox("Bold");
        panell.add(bold);
        italic = new JCheckBox("Italic");
        panell.add(italic);

        panel2 = new JPanel();
        left = new JButton("Left");
        panel2.add(left);
        right = new JButton("Right");
        panel2.add(right);

        panel3 = new JPanel();
        panel3.setBackground(Color.white);
        text = new JTextArea("Welcome to java");
        panel3.add(text);
        
        panel4  = new JPanel(new GridLayout(3,3));
        panel4.setBorder(line);
        red = new JRadioButton("Red");
        panel4.add(red);
        green = new JRadioButton("Green");
        panel4.add(green);
        blue = new JRadioButton("Blue");
        panel4.add(blue);
        

        contentPane.add(panell, BorderLayout.EAST);
        contentPane.add(panel2, BorderLayout.SOUTH);
        contentPane.add(panel3, BorderLayout.CENTER);
        contentPane.add(panel4, BorderLayout.WEST);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

}
