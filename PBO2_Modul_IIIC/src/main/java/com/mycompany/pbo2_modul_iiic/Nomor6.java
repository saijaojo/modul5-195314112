/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iiic;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author ASUS
 */
public class Nomor6 extends JDialog {

    public Nomor6() {
        initTextAreaScroll();
        initGambarComboBox();
        setTitle("ComboBoxDemo");
        setSize(300, 200);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
    
    public void initGambarComboBox() {
        String[] negara = {"English", "Canada", "Indonesia", "Malaysia"};
        this.setLayout(null);
        JComboBox comboBox_negara = new JComboBox(negara);
        comboBox_negara.setBounds(1, 1, 400, 20);
        this.add(comboBox_negara);

        
        ImageIcon img = new ImageIcon("canada1.png");
        JLabel labelGambar = new JLabel(img);
        labelGambar.setBounds(2, 10, 100, 100);
        this.add(labelGambar);

        JLabel labelNamaGambar = new JLabel("Canada");
        labelNamaGambar.setBounds(2, 10, 250, 200);
        this.add(labelGambar);
    }

    public void initTextAreaScroll() {
        JTextArea textArea_deskripsi = new JTextArea("Negara Canada");
        textArea_deskripsi.setBounds(100, 20, 100, 100);
        JScrollPane scroll = new JScrollPane(textArea_deskripsi, JScrollPane.
                VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(100, 20, 100, 100);
        this.add(scroll);
    }

    public static void main(String[] args) {
        Nomor6 test = new Nomor6();


    }
}
