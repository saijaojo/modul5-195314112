/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo_modul4praktikum;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JTextField;

/**
 *
 * @author asus
 */
public class Tugas1 extends JFrame implements ActionListener{
    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 210;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int LABEL_WIDTH = 80;
    private static final int LABEL_HEIGHT = 30;
     private static final int FIELD_WIDTH = 150;
    private static final int FIELD_HEIGHT = 30;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JLabel label_bil1;
    private JLabel label_bil2;
    private JLabel label_hasil;
    private JTextField text_bil1;
    private JTextField text_bil2;
    private JTextField text_hsl;
    private JButton button_jumlah;
    
    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
    }

    public Tugas1() {
       Container contenPane =  getContentPane();
       contenPane.setLayout(null);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        label_bil1 = new JLabel("Bilangan 1");
        label_bil1.setBounds(20, 15, LABEL_WIDTH, LABEL_HEIGHT);
        contenPane.add(label_bil1);
        
        label_bil2 = new JLabel("Bilangan 2");
        label_bil2.setBounds(20, 50, LABEL_WIDTH, LABEL_HEIGHT);
        contenPane.add(label_bil2);
        
        label_hasil = new JLabel("Hasil");
        label_hasil.setBounds(20, 85, LABEL_WIDTH, LABEL_HEIGHT);
        contenPane.add(label_hasil);
        
        text_bil1 = new JTextField();
        text_bil1.setBounds(140, 15, FIELD_WIDTH, FIELD_HEIGHT);
        contenPane.add(text_bil1);
        
        text_bil2 = new JTextField();
        text_bil2.setBounds(140, 50, FIELD_WIDTH, FIELD_HEIGHT);
        contenPane.add(text_bil2);
        
        text_hsl = new JTextField();
        text_hsl.setBounds(140, 85, FIELD_WIDTH, FIELD_HEIGHT);
        contenPane.add(text_hsl);
        
        button_jumlah = new JButton("Jumlah");
        button_jumlah.setBounds(180, 125, BUTTON_WIDTH, BUTTON_HEIGHT);
        button_jumlah.addActionListener(this);
        contenPane.add(button_jumlah);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clikedButton = (JButton)e.getSource();
        
        JRootPane roonPane = clikedButton.getRootPane();
        Frame frame = (JFrame)roonPane.getParent();
        String buttonText = clikedButton.getText();
        
        double bil1 = Double.parseDouble(text_bil1.getText());
        double bil2 = Double.parseDouble(text_bil2.getText());
        
        double hasil = bil1 + bil2;
        
        text_hsl.setText(""+ hasil);
    }
}
