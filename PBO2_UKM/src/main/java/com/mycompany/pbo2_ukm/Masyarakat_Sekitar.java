/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_ukm;

/**
 *
 * @author asus
 */
public class Masyarakat_Sekitar extends Penduduk{
    private String nomor;

    public Masyarakat_Sekitar() {
    }

    public Masyarakat_Sekitar(String nomor, String nama, String tempat_tanggal_lahir) {
        super(nama, tempat_tanggal_lahir);
        this.nomor = nomor;
    }
    
    void setNomor (String dataNomor){
        this.nomor = dataNomor;
    }
    String getNomor(){
        return nomor;
    }
    
    

    @Override
    double hitungluran() {
        double iuran = Double.parseDouble(nomor);
        
        return iuran*100;
    }
    
    
}
