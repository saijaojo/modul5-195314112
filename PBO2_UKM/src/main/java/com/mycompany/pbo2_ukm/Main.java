/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_ukm;

import javax.swing.JOptionPane;

/**
 *
 * @author asus
 */
public class Main {

    public static void main(String[] args) {
        UKM ukm = new UKM("UKM BAHAGIA");
        
        System.out.println("====================================== "+ukm.getnamaUnit()+" ================================");
        System.out.println("=======================================================================================");

        Mahasiswa ketua = new Mahasiswa();
        String nim = JOptionPane.showInputDialog(null, "Masukan NIM Ketua");
        ketua.setNim(nim);
        String nama = JOptionPane.showInputDialog(null, "Masukan Nama Ketua");
        ketua.setNama(nama);
        String ttl = JOptionPane.showInputDialog(null, "Masukan tepat tanggal lahir ketua");
        ketua.setTempat_tanggal_lahir(ttl);

        Mahasiswa sekertaris = new Mahasiswa();
        nim = JOptionPane.showInputDialog(null,"Masukan NIM Sekertaris");
        sekertaris.setNim(nim);
        nama = JOptionPane.showInputDialog(null,"Masukan Nama Sekertaris");
        sekertaris.setNama(nama);
        ttl = JOptionPane.showInputDialog(null,"Masukan tempat tanggal lahir sekertaris");
        sekertaris.setTempat_tanggal_lahir(ttl);
        
        System.out.println("NIM Ketua                       : "+ ketua.getNim());
        System.out.println("Nama Ketua                      : "+ ketua.getNim());
        System.out.println("Tempat tanggal lahir ketua      : "+ketua.getgetTempat_tanggal_lahir());
        System.out.println("Iuran                           : "+ ketua.hitungluran());
        System.out.println("----------------------------------------------------------------------------------");
        System.out.println("NIM Sekertaris                  : "+ sekertaris.getNim());
        System.out.println("Nama Sekertaris                 : "+sekertaris.getNama());
        System.out.println("Tempat tanggal lahir Sekertaris : "+sekertaris.getgetTempat_tanggal_lahir());
        System.out.println("Iuaran                          : "+sekertaris.hitungluran());
        
        System.out.println("===================================================================================");
        System.out.println("======================================= Angota ====================================");
        System.out.println("Nim atau Nomor \t Nama \t \t Tempat tanggal lahir \t Iuran ");
        System.out.println("-----------------------------------------------------------------------------------");
        

        int a = Integer.parseInt(JOptionPane.showInputDialog(null, "Masukan jumlah anggota baru dari Mahasiswa"));
        Mahasiswa[] mhs = new Mahasiswa[a];

        for (int i = 0; i < a; i++) {
            mhs[i] = new Mahasiswa();
            nim = JOptionPane.showInputDialog(null, "Masukan NIM Anggota ");
            mhs[i].setNim(nim);
            nama = JOptionPane.showInputDialog(null, "Masukan Nama Anggota");
            mhs[i].setNama(nama);
            ttl = JOptionPane.showInputDialog(null, "Masukan tempat tanggal lahir");
            mhs[i].setTempat_tanggal_lahir(ttl);
            
            System.out.println(""+mhs[i].getNim()+" \t "+mhs [i].getNama()+" \t "
                    + mhs [i].getgetTempat_tanggal_lahir()+" \t "+ mhs [i].hitungluran());
            System.out.println("-------------------------------------------------------------------------------------");
        }

        int b = Integer.parseInt(JOptionPane.showInputDialog(null, "Masukan jumlah anggota baru dari Masyarakat"));
        Masyarakat_Sekitar[] ms = new Masyarakat_Sekitar[b];
        for (int j = 0; j < b; j++) {
            ms[j] = new Masyarakat_Sekitar();
            String nomor = JOptionPane.showInputDialog(null, "Masukan Nomor Anggota");
            ms[j].setNomor(nomor);
            nama = JOptionPane.showInputDialog(null, "Masukan Nama Anggota");
            ms[j].setNama(nama);
            ttl = JOptionPane.showInputDialog(null, "Masukan tempat tanggal lahir");
            
            System.out.println(""+ms [j].getNomor()+" \t "+ms [j].getNama()
                    +" \t "+ms [j].getgetTempat_tanggal_lahir()+" \t "+ ms [j].hitungluran());
            System.out.println("------------------------------------------------------------------------------------");
        }
    }
}
