/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author asus
 */
public abstract class Penduduk {
    private String name;
    private String tempat_Tanggal_Lahir;

    public Penduduk() {
    }

    public Penduduk(String name, String tempat_Tanggal_Lahir) {
        this.name = name;
        this.tempat_Tanggal_Lahir = tempat_Tanggal_Lahir;
    }
    
    String getNama (){
        return name;
    }
    void setNama (String dataNama){
        this.name = dataNama;
    }
    String getTempat_Tanggal_Lahir (){
        return tempat_Tanggal_Lahir;
    }
    void setTempat_Tanggal_Lahir (String dataTempat_Tanggal_Lahir){
        this.tempat_Tanggal_Lahir =dataTempat_Tanggal_Lahir;
    }
    public abstract double hitungIuran();
}
