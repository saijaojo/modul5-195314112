/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author asus
 */
public class Masyarakat_Sekitar  extends Penduduk{
    private String nomor;

    public Masyarakat_Sekitar() {
    }

    public Masyarakat_Sekitar(String nomor) {
        this.nomor = nomor;
    }

    public Masyarakat_Sekitar(String nomor, String name, String tempat_Tanggal_Lahir) {
        super(name, tempat_Tanggal_Lahir);
        this.nomor = nomor;
    }
    String getNomor (){
        return nomor;
    }
    void setNomot (String dataNomor){
        this.nomor = dataNomor;
    }

    @Override
    public double hitungIuran() {
        double nilai = Double.parseDouble(nomor);
        double iuaran = nilai * 100;
        return iuaran;
    } 
    
}
