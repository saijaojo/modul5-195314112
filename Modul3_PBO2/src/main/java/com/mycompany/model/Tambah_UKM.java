/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class Tambah_UKM extends JDialog implements ActionListener {

    private JLabel label_title;
    private JButton button_ok;
    private JLabel label_namaUnit;
    private JLabel label_ketua;
    private JLabel label_sekretaris;
    private JLabel label_penduduk;
    private JComboBox comBox_ketua;
    private JComboBox comBox_sekretaris;
    private JTextField textField_namaUnit;
    private JTextField textField_ketua;
    private JTextField textField_sekretaris;
    private JComboBox comBox_penduduk;
    
    private String pendudukArray[] = {"Aldi", "Kayeta", "Pandu", "Jo"};
   

    public Tambah_UKM() {
        init();
    }

    public void init() {
        this.setLayout(null);
        
        label_title = new JLabel("Form Tambah UKM");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);
        
        label_namaUnit = new JLabel("Nama unit\t :");
        label_namaUnit.setBounds(80, 60, 100, 50);
        this.add(label_namaUnit);
        textField_namaUnit = new JTextField();
        textField_namaUnit.setBounds(180, 75, 180, 20);
        this.add(textField_namaUnit);
        
        label_ketua = new JLabel("Ketua \t :");
        label_ketua.setBounds(80, 100, 100, 50);
        this.add(label_ketua);
        String list = "";
        for (int i = 0; i < mainFrame.anggota.length; i++) {
            if (mainFrame.anggota[i] != null) {
                list = mainFrame.anggota[i].getNama();
            }
        }
        comBox_ketua = new JComboBox();
        comBox_ketua.setBounds(180, 115, 180, 20);
        this.add(comBox_ketua);

        
        label_sekretaris = new JLabel("Sekretaris\t :");
        label_sekretaris.setBounds(80, 140, 100, 50);
        this.add(label_sekretaris);
        textField_sekretaris = new JTextField();
        textField_sekretaris.setBounds(180, 155, 180, 20);
        this.add(textField_sekretaris);
        
        label_penduduk = new JLabel("Penduduk \t :");
        label_penduduk.setBounds(80, 180, 100, 50);
        this.add(label_penduduk);
        comBox_penduduk = new JComboBox(pendudukArray);
        comBox_penduduk.setBounds(180, 195, 180, 20);
        this.add(comBox_penduduk);
        
        
        button_ok = new JButton("OK");
        button_ok.setBounds(150, 250, 100, 30);
        this.add(button_ok);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
