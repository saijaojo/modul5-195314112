/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;



public class mainFrame extends JFrame implements ActionListener{


    private JMenuBar menuBar;
    private JMenuItem menuItemEdit_tambahMhs;
    private JMenuItem menuItemEdit_tambahMasyarakat;
    private JMenuItem menuItemEdit_tambahUKM; 
    private JMenuItem menuItemFile_lihatData;
    private JMenuItem menuItemFile_lihatMasyarakat;
    private JMenuItem menuItemFile_exit;
    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenu menu_Help;
    
    public static UKM ukm = new UKM();
    public static Mahasiswa mhs = new Mahasiswa();
    public static Masyarakat_Sekitar masya = new Masyarakat_Sekitar();
    public static Penduduk[] anggota = new Penduduk[5];
    public static int jumlah = 0;
    
    
    public mainFrame() {
        initComponents();
    }

    private void initComponents() {
        menuBar = new JMenuBar();
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menu_Help = new JMenu("Help");

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        this.setJMenuBar(menuBar);

        menuItemEdit_tambahMhs = new JMenuItem("Tambah Mahasiswa");
        menuItemEdit_tambahMasyarakat = new JMenuItem("Tambah Masyarakat");
        menuItemEdit_tambahUKM = new JMenuItem("Tambah UKM");
        menuItemFile_lihatData = new JMenuItem("Lihat Data");
        menuItemFile_lihatMasyarakat = new JMenuItem("Data Masyarakat");
        menuItemFile_exit= new JMenuItem("Exit");

        menu_Edit.add(menuItemEdit_tambahMhs);
        menu_Edit.add(menuItemEdit_tambahMasyarakat);
        menu_Edit.add(menuItemEdit_tambahUKM);
        
        menu_File.add(menuItemFile_lihatMasyarakat);
        menu_File.add(menuItemFile_lihatData);
        menu_File.add(menuItemFile_exit);
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        
        
        menuItemFile_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        menuItemEdit_tambahMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tambah_Mahasiswa addMhs = new Tambah_Mahasiswa();
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);
            }
        });
        
        menuItemEdit_tambahMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tambah_Masyarakat addMasyarakat = new Tambah_Masyarakat();
                addMasyarakat.setSize(400, 300);
                addMasyarakat.setVisible(true);
            }
        });
        
        menuItemEdit_tambahUKM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tambah_UKM addUKM = new Tambah_UKM();
                addUKM.setSize(400, 460);
                addUKM.setVisible(true);
            }
        });
        
        menuItemFile_lihatData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tabel_Mahasiswa mhsTable = new Tabel_Mahasiswa();
                mhsTable.setSize(300, 400);
                mhsTable.setVisible(true);
            }
        });
        
        menuItemFile_lihatMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tabel_Masyarakat masyaTable = new Tabel_Masyarakat();
                masyaTable.setSize(300, 400);
                masyaTable.setVisible(true);
            }
        });

    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame main = new mainFrame();
                main.setSize(400, 500);
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
}

